# Setting up Miniconda3
### Download  Miniconda3 package manager from the [Miniconda webpage](https://docs.conda.io/en/latest/miniconda.html)

### Install Miniconda3
```
cd path/to/Miniconda3-latest-*.sh
bash Miniconda3-latest-*.sh
```

### Press ENTER to review the license agreement and type `yes` to agree to the license terms

### Confirm Miniconda3 to be installed in your user directory (`/home/user/miniconda3`) or specify a different location by providing the respective path:
```
path/to/miniconda3
```

### When prompted if the installer should initialize Miniconda3 by running conda init - type `yes`

### After the installation, source the .bashrc or .bash_profile file located in your home directory(whichever applies) - type `ls -a` to see which one you have
```
source ~/.bashrc
```
### or
```
source ~/.bash_profile
```

### Create virtual environment
```
conda create -n <env_name> <env_package>
```

### If you wish not to activate the base environment upon login in, execute the following code:
```
conda config --set auto_activate_base false
```

### Install packages
```
conda activate <env_name>
conda install <env_package>
```

### For further tips and tricks for conda use the [conda cheatsheet](https://docs.conda.io/projects/conda/en/4.6.0/_downloads/52a95608c49671267e40c689e0bc00ca/conda-cheatsheet.pdf)